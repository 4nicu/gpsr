#!/usr/bin/env python

"""
    gpsr_main.py 
  - Main flow of the General Purpose Service Robot package

"""

import roslib; roslib.load_manifest('pi_speech_tutorial')
import rospy
from std_msgs.msg import String, Header
from kobuki_msgs.msg import DigitalInputEvent

import actionlib
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose, PoseWithCovarianceStamped, Point, Quaternion, Twist, PoseStamped, PoseWithCovariance
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseActionGoal
from tf.transformations import quaternion_from_euler

from sound_play.libsoundplay import SoundClient

point = 1
original = 0
place = 1
message = "none"
start = 0
num_order = 0
step = 0
stop = 0
food_type = "none"

locations = dict()
delivery_loc = dict()
object_loc = dict()

digital_in = [False, False, False, False]

class GPSR_main:
    def __init__(self):
        rospy.on_shutdown(self.cleanup)
          
        self.voice = rospy.get_param("~voice", "voice_don_diphone")
        self.wavepath = rospy.get_param("~wavepath", "")
	rospy.loginfo("1")
        
        # Create the sound client object
        self.soundhandle = SoundClient()
        rospy.sleep(1)
        self.soundhandle.stopAll()
	rospy.loginfo("2")

        # Subscribe to the recognizer output
        rospy.Subscriber('/recognizer/output', String, self.identify)
	
	# GPSR topics
	rospy.Subscriber('/gpsr_location1', String, self.loc1)
	rospy.Subscriber('/gpsr_location2', String, self.loc2)
	rospy.Subscriber('/gpsr_begin', String, self.begin)
	rospy.Subscriber('/gpsr_command', String, self.get_command)
	rospy.Subscriber('/gpsr_name', String, self.get_name)
	self.pass_action = rospy.Publisher('gpsr_action' String)
	self.begin_speech = rospy.Publisher('gpsr_speech', String)
	rospy.loginfo("3")

	# Human detection
	rospy.Subscriber('/people_detect', String, self.detection_cb)
	rospy.Subscriber('/rotate_done', String, self.rotating_cb)
	self.start_detection = rospy.Publisher('detect_start' String)

	# Publisher to manually control the robot
        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)
	
	# Publisher for initial position
        self.p = PoseWithCovarianceStamped()
	self.msg = PoseWithCovariance()
	self.init_pose_pub = rospy.Publisher('initialpose', PoseWithCovarianceStamped)

	# Subscribe to the move_base action server
        self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
        rospy.loginfo("Waiting for move_base action server...")
        # Wait for the action server to become available
        #self.move_base.wait_for_server(rospy.Duration(120))
        rospy.loginfo("Connected to move base server")
	rospy.loginfo("4")

        # Announce that we are ready for input
        self.soundhandle.playWave(self.wavepath + "/R2D2a.wav")
        rospy.sleep(1)
        self.soundhandle.say("initiated", self.voice)
	rospy.sleep(1)
	rospy.loginfo("5")

	# Ordering location
	self.msg.pose = Pose( Point(1.37290459773, 0.4268675966, 0.0), Quaternion(0.0, 0.0, -0.984356578764, 0.176187757361) )
	self.msg.covariance = [0.0030628982822256123, 0.000771303527867806, 0.0, 0.0, 0.0, 0.0, 0.000771303527867806, 0.002912862584606074, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0049039823453215]

	self.p.pose = self.msg
	self.p.header.frame_id = 'map'
	self.p.header.stamp = rospy.Time.now()

	self.init_pose_pub.publish(self.p)
	rospy.loginfo("8")
	global locations
	rospy.loginfo("9")

	# COMMAND POINT (at hallway)
	locations['wait'] = Pose(Point(4.73395013809, -0.386209726334, 0.0), Quaternion(0.0, 0.0, 0.936102753852, 0.35172664703))	

	# ROOMS
	locations['kitchen_1'] = Pose(Point(1.69450640678,  4.57174491882, 0.0), Quaternion(0.0, 0.0, -0.619830320873, 0.784735862138))
	locations['kitchen_2'] = Pose(Point(-0.547351121902, 1.92031705379, 0.0), Quaternion(0.0, 0.0, -0.0345912423359, 0.999401543902))
	locations['living_1'] = Pose(Point(7.1051440239, 3.64482307434, 0.0), Quaternion(0.0, 0.0, -0.687214918774, 0.72645416608)) 
	locations['living_2'] = Pose(Point(7.77338981628, 4.70108270645, 0.0), Quaternion(0.0, 0.0, 0.658250737748, 0.752798755482)) 
	locations['living_3'] = Pose(Point(5.20818758011, 7.46029233932, 0.0), Quaternion(0.0, 0.0, 0.0656105128088, 0.997845308958)) 
	locations['living_4'] = Pose(Point(3.3112308979, 5.86512374878, 0.0), Quaternion(0.0, 0.0, 0.755453773828, 0.655201950248)) 
	locations['hallway_1'] = Pose(Point(8.61408615112, -2.56339788437, 0.0), Quaternion(0.0, 0.0, 0.934327965526, 0.356414439713)) 
	locations['bedroom_1'] = Pose(Point(10.015247345, 1.03650796413, 0.0), Quaternion(0.0, 0.0, 0.374269412995, 0.927320012992)) 	

	# ENTRANCE & EXIT
	locations['A'] = Pose(Point(-2.24048652306, 1.25864007347, 0.0), Quaternion(0.0, 0.0, -0.995923906795, 0.0901974050315)) 
	locations['B'] = Pose(Point(-1.53899611599, -0.226521802626, 0.0), Quaternion(0.0, 0.0, -0.0572611971101, 0.998359231592)) 

	# DOORS
	locations['C'] = Pose(Point(-2.24048652306, 1.25864007347, 0.0), Quaternion(0.0, 0.0, -0.995923906795, 0.0901974050315)) 
	locations['D'] = Pose(Point(-2.24048652306, 1.25864007347, 0.0), Quaternion(0.0, 0.0, -0.995923906795, 0.0901974050315)) 
	locations['E'] = Pose(Point(-2.24048652306, 1.25864007347, 0.0), Quaternion(0.0, 0.0, -0.995923906795, 0.0901974050315)) 
	locations['F'] = Pose(Point(-2.24048652306, 1.25864007347, 0.0), Quaternion(0.0, 0.0, -0.995923906795, 0.0901974050315)) 	

	# HUMANS
	rospy.loginfo("End of locations init")
	self.goal = MoveBaseGoal()
        '''rospy.loginfo("Starting navigation test")
	rospy.loginfo("15")
        self.soundhandle.say("Please give command", self.voice)'''
	rospy.sleep(2)

	global start
	global step

	while not rospy.is_shutdown():
		self.goal.target_pose.header.frame_id = 'map'
		self.goal.target_pose.header.stamp = rospy.Time.now()
		if stop == 1:
			self.cmd_vel_pub.publish(Twist())
		elif stop == 0:
			if start == 0:
				# MOVE TO COMMAND POINT FROM OUTSIDE ARENA
				rospy.loginfo("Moving to command point")
				self.goal.target_pose.pose = object_loc['wait']
				self.move_base.send_goal(self.goal)
				waiting = self.move_base.wait_for_result(rospy.Duration(300))
				if waiting == 1:
					rospy.loginfo("Waiting for command...")
					self.soundhandle.say("To ask a question, please say my name:", self.voice)
					rospy.sleep(1)
					self.soundhandle.say("Kame rider", self.voice)
					rospy.sleep(2)
					self.soundhandle.say("Please give command", self.voice)
					waiting = 0
					if trigger_seq == 1:
						start = 2

			elif start == 2:
				rospy.loginfo("Waiting for command...")
				# MOVE TO FIRST LOCATION FOR THE ROOM
				self.goal.target_pose.pose = object_loc['A']
				self.move_base.send_goal(self.goal)
				waiting = self.move_base.wait_for_result(rospy.Duration(300))
				while waiting == 0:
					pass
				waiting = 0
				self.soundhandle.say("Begin human detection sequence", self.voice)
				rospy.loginfo("Begin human detection sequence")
				# HUMAN DETECTION
				self.start_detection.publish('start')
				if destination1 == 'kitchen': 
					while searching == 0:
						pass
					searching = 0
					if human_detected == 1:
						start = 3
					elif human_detected == 0:	
						self.goal.target_pose.pose = location['kitchen_2']
						self.move_base.send_goal(self.goal)
						waiting = self.move_base.wait_for_result(rospy.Duration(300))
						while waiting == 0:
							pass
						waiting = 0
						self.start_detection.publish('start')
						while searching == 0:
							pass
						searching = 0
						if human_detected == 1:
							start = 3
				elif destination1 == 'living room': 
					while searching == 0:
						pass
					searching = 0
					if human_detected == 1:
						start = 3
					elif human_detected == 0:	
						self.goal.target_pose.pose = location['kitchen_2']
						self.move_base.send_goal(self.goal)
						waiting = self.move_base.wait_for_result(rospy.Duration(300))
						while waiting == 0:
							pass
						waiting = 0
						self.start_detection.publish('start')
						while searching == 0:
							pass
						searching = 0
						if human_detected == 1:
							start = 3
						elif human_detected == 0:	
							self.goal.target_pose.pose = location['kitchen_3']
							self.move_base.send_goal(self.goal)
							waiting = self.move_base.wait_for_result(rospy.Duration(300))
							while waiting == 0:
								pass
							waiting = 0
							self.start_detection.publish('start')
							while searching == 0:
								pass
							searching = 0
							if human_detected == 1:
								start = 3
							elif human_detected == 0:	
								self.goal.target_pose.pose = location['kitchen_4']
								self.move_base.send_goal(self.goal)
								waiting = self.move_base.wait_for_result(rospy.Duration(300))
								while waiting == 0:
									pass
								waiting = 0
								self.start_detection.publish('start')
								while searching == 0:
									pass
								searching = 0
								if human_detected == 1:
									start = 3
				elif destination1 == 'bedroom': 
					while searching == 0:
						pass
					searching = 0
					if human_detected == 1:
						start = 3
				elif destination1 == 'hallway': 
					while searching == 0:
						pass
					searching = 0
					if human_detected == 1:
						start = 3
				rospy.sleep(2)

			elif start == 3:
				rospy.loginfo("Human detected... Activating speech")
				self.pass_action.publish(command_pub)
				while trigger_seq == 1:
					pass
				trigger_seq = 2
				rospy.loginfo("Moving to second location")
				if destination2 == 'exit'
					self.soundhandle.say("Leaving the arena", self.voice)
					self.goal.target_pose.pose = object_loc['A']
					self.move_base.send_goal(self.goal)
					waiting = self.move_base.wait_for_result(rospy.Duration(300))
					if waiting == 1:
						start = 4
				else 
					self.goal.target_pose.pose = object_loc['A']
					self.move_base.send_goal(self.goal)
					waiting = self.move_base.wait_for_result(rospy.Duration(300))
					# GIVE COMMAND FOR NEXT ACTION
					self.pass_action.publish(command_pub)
					if trigger_seq == 'done':
						self.soundhandle.say("Leaving the arena", self.voice)
						self.goal.target_pose.pose = locations['A']
						self.move_base.send_goal(self.goal)
						self.move_base.wait_for_result(rospy.Duration(300))
						if waiting == 1:
							start = 4
          		rospy.Rate(5).sleep()

    def update_initial_pose(self, initial_pose):
        self.initial_pose = initial_pose
	global original
	if original == 0:
            self.origin = self.initial_pose.pose.pose
            #global original
            original = 1

    def identify(self, msg):
	'''global step
	global start
	global delivery_loc
	global object_loc'''

    def loc1(self, data_1):
	global destination1 = data_1.data		
	rospy.loginfo("Location 1 received")
	if data_1 == 'kitchen': 
		object_loc['A'] = locations['kitchen_1']
		rospy.loginfo("KITCHEN set as FIRST location")
	elif data_1 == 'living room': 
		object_loc['A'] = locations['living_1']
		rospy.loginfo("LIVING ROOM set as FIRST location")
	elif data_1 == 'bedroom': 
		object_loc['A'] = locations['bedroom_1']
		rospy.loginfo("BEDROOM set as FIRST location")
	elif data_1 == 'hallway': 
		object_loc['A'] = locations['hallway_1']
		rospy.loginfo("HALLWAY set as FIRST location")
		
    def loc2(self, data_2):
	global destination2 = data_2.data
	rospy.loginfo("Location 2 received")
	if data_2 == 'return':
		object_loc['B'] = locations['wait']
		rospy.loginfo("COMMAND POINT set as SECOND location")
	elif data_2 == 'exit':
		object_loc['B'] = locations['B']
		rospy.loginfo("LEAVING ARENA AFTER FIRST SEQUENCE")
	elif data_2 == 'kitchen':
		object_loc['B'] = locations['kitchen_1']
		rospy.loginfo("KITCHEN set as SECOND location")
	elif data_2 == 'living room':
		object_loc['B'] = locations['living_1']
		rospy.loginfo("LIVING ROOM set as SECOND location")
	elif data_2 == 'bedroom':
		object_loc['B'] = locations['bedroom_1']
		rospy.loginfo("BEDROOM set as SECOND location")
	elif data_2 == 'hallway':
		object_loc['B'] = locations['hallway_1']
		rospy.loginfo("HALLWAY set as SECOND location")
		
    def begin(self, data_3):
	global trigger_seq = 0
	if data_3.data == 'start':
		trigger_seq = 1
	elif data_3.data == 'done':
		trigger_seq = 0

    def get_command(self, data_4):
	global command_pub = data_4.data

    def detection_cb(self, data_5):
	global human_detected
	if data_5.data == 'people_found':
		human_detected = 1
	elif data_5.data == 'not_found':
		human_detected = 0

    def rotating_cb(self, data_6):
	global searching = 0
	if data_6.data == 'done':
		searching = 1

    def DigitalInputEventCallback(self, data):
    	global digital_in
	global stop
    	digital_in = data.values
	if digital_in[3] == True:	# emergency button pressed
	    stop = 1
	    rospy.loginfo("Button RED pressed !!!")
	elif digital_in[3] == False:	# emergency button not pressed
	    stop = 0
	    rospy.loginfo("Safe")

    def cleanup(self):
        rospy.loginfo("Shutting down navigation	....")
	self.move_base.cancel_goal()
        self.cmd_vel_pub.publish(Twist())

if __name__=="__main__":
    rospy.init_node('GPSR_main')
    try:
        GPSR_main()
        #rospy.spin()
    except:
        pass


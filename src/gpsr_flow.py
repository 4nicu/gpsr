#!/usr/bin/env python

"""
    basic_function.py - peforms tasks for basic functionality of RoboCup Japan Open.

"""

import roslib; roslib.load_manifest('pi_speech_tutorial')
import rospy
from std_msgs.msg import String, Header
from kobuki_msgs.msg import DigitalInputEvent

import actionlib
from actionlib_msgs.msg import *
from geometry_msgs.msg import Pose, PoseWithCovarianceStamped, Point, Quaternion, Twist, PoseStamped, PoseWithCovariance
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseActionGoal
from tf.transformations import quaternion_from_euler

from sound_play.libsoundplay import SoundClient

point = 1
original = 0
place = 1
message = "none"
start = 0
num_order = 0
step = 0
stop = 0
count = 0

digital_in = [False, False, False, False]

class B_Function:
    def __init__(self):
        rospy.on_shutdown(self.cleanup)
          
        self.voice = rospy.get_param("~voice", "voice_don_diphone")
        self.wavepath = rospy.get_param("~wavepath", "")
        
        # Create the sound client object
        self.soundhandle = SoundClient()
        rospy.sleep(1)
        self.soundhandle.stopAll()

        # Announce that we are ready for input
        self.soundhandle.playWave(self.wavepath + "/R2D2a.wav")
        rospy.sleep(1)

	## IF GATE OPEN ##
        #self.soundhandle.say("initiated", self.voice)
       	#self.soundhandle.say("Please give command", self.voice)
	#rospy.sleep(1)

        # Subscribe to the recognizer output
        rospy.Subscriber('/recognizer/output', String, self.identify)

    	# Subscribe to the /mobile_base/events/digital_input topic to receive digital input
    	rospy.Subscriber('/mobile_base/events/digital_input', DigitalInputEvent, self.DigitalInputEventCallback)

	# Publisher to manually control the robot
        self.cmd_vel_pub = rospy.Publisher('cmd_vel', Twist)

	# Subscribe to the move_base action server
        self.move_base = actionlib.SimpleActionClient("move_base", MoveBaseAction)
        rospy.loginfo("Waiting for move_base action server...")
        # Wait for the action server to become available
        self.move_base.wait_for_server(rospy.Duration(120))
        rospy.loginfo("Connected to move base server")

        # A variable to hold the initial pose of the robot to be set by the user in RViz
        self.p = PoseWithCovarianceStamped()
	self.msg = PoseWithCovariance()
	self.init_pose_pub = rospy.Publisher('initialpose', PoseWithCovarianceStamped)

	self.goal = MoveBaseGoal()
        rospy.loginfo("Starting navigation test")
	rospy.sleep(2)
	rospy.loginfo("1")

	# Not sure can work or not, need to test
	# Record initial coordinate manually (rostopic echo
	# /initialpose)
	# No need to use rviz every time
	self.msg.pose = Pose( Point(-3.40219233937, -6.32186664976, 0.0), Quaternion(0.0, 0.0, 0.951225493796, 0.308496450471) )
	self.msg.covariance = [0.003969935530228952, -0.001525861164978437, 0.0, 0.0, 0.0, 0.0, -0.001525861164978437, 0.0025626120123760643, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.002177038837217916]
	self.p.pose = self.msg
	self.p.header.frame_id = 'map'
	self.p.header.stamp = rospy.Time.now()

	rospy.loginfo("2")
	self.init_pose_pub.publish(self.p)
	rospy.loginfo("3")

	locations = dict()

	# EXIT POINT (Outside arena)
	locations['A'] = Pose(Point(-7.40689392193, -2.40678079677, 0.0), Quaternion(0.0, 0.0, -0.986009015573, 0.166691995033))

	# Object retrieval
	locations['B'] = Pose(Point(0.271629548179, 4.048870413, 0.0), Quaternion(0.0, 0.0, 0.248766361017, 0.968563522763))

	# Object placement location
	locations['C'] = Pose(Point(-0.851690262761, 0.517672961243, 0.0), Quaternion(0.0, 0.0, 0.144314539783, 0.989531865888))

	# Adjacent room
	locations['D'] = Pose(Point(-1.72650769245, 3.48488303722, 0.0), Quaternion(0.0, 0.0, 0.68956627004, 0.724222589556))
	

	global start
	global step

	while not rospy.is_shutdown():
	  self.goal.target_pose.header.frame_id = 'map'
	  self.goal.target_pose.header.stamp = rospy.Time.now()

	  if stop == 1:
            self.cmd_vel_pub.publish(Twist())
	  elif stop == 0:
	    if start == 1:
            	rospy.Rate(3).sleep()


    def update_initial_pose(self, initial_pose):
        self.initial_pose = initial_pose
	global original
	if original == 0:
            self.origin = self.initial_pose.pose.pose
            #global original
            original = 1

    def identify(self, msg):
	rospy.loginfo(msg.data)
	global step
	#global locations
	global start
	global count
	self.goal.target_pose.header.frame_id = 'map'
	self.goal.target_pose.header.stamp = rospy.Time.now()

	locations = dict()

	# EXIT POINT (Outside arena)
	locations['A'] = Pose(Point(-7.40689392193, -2.40678079677, 0.0), Quaternion(0.0, 0.0, -0.986009015573, 0.166691995033))

	# Object retrieval
	locations['B'] = Pose(Point(0.271629548179, 4.048870413, 0.0), Quaternion(0.0, 0.0, 0.248766361017, 0.968563522763))

	# Object placement location
	locations['C'] = Pose(Point(-0.851690262761, 0.517672961243, 0.0), Quaternion(0.0, 0.0, 0.144314539783, 0.989531865888))

	# Adjacent room
	locations['D'] = Pose(Point(-1.72650769245, 3.48488303722, 0.0), Quaternion(0.0, 0.0, 0.68956627004, 0.724222589556))
	

	if step == 0:
	    #if msg.data == 'turtlebot, start your journey' or msg.data == 'journey' or msg.data == 'start your journey':
		#### navigate to command point ####
		#rospy.sleep(1)
		rospy.loginfo("initiated")
		self.soundhandle.say("Moving into arena", self.voice)
		self.goal.target_pose.pose = Pose(Point(-3.08001204845, 0.774724544203, 0.0), Quaternion(0.0, 0.0, 0.432460350894, 0.901652951476))
		self.move_base.send_goal(self.goal)
		waiting = self.move_base.wait_for_result(rospy.Duration(300))
		if waiting == 1:
			self.soundhandle.say("Reached destination", self.voice)
			rospy.sleep(1)
			self.soundhandle.say("Awaiting command", self.voice)		
			rospy.loginfo("4")
			step = 1

	if step == 1:
	    	rospy.loginfo("check for command")
		#### Case 1: Locating person ####
		if msg.data == 'argentina':
			rospy.loginfo("case 1")
			self.soundhandle.say("going to first destination", self.voice)
	  		self.goal.target_pose.pose = locations['D']
			self.move_base.send_goal(self.goal)
			waiting = self.move_base.wait_for_result(rospy.Duration(300))
			if waiting == 1:
                    		self.soundhandle.say("Location reached", self.voice)
                    		rospy.sleep(1)
                    		self.soundhandle.say("Leaving the arena", self.voice)
				self.goal.target_pose.pose = locations['A']
				self.move_base.send_goal(self.goal)
				self.soundhandle.say("End of routine", self.voice)
                    		start = 150
		#### Case 2: Grab object and release ####
		if msg.data == 'brazil':
			rospy.loginfo("case 2")
			self.soundhandle.say("going to first destination", self.voice)
			self.goal.target_pose.pose = locations['B']
			self.move_base.send_goal(self.goal)
			waiting = self.move_base.wait_for_result(rospy.Duration(300))

			self.soundhandle.say("Destination reached", self.voice)
			rospy.sleep(1)
			self.soundhandle.say("Grabbing object", self.voice)
			rospy.sleep(1)
			self.soundhandle.say("Moving to next destination", self.voice)
			
			self.goal.target_pose.pose = locations['c']
			self.move_base.send_goal(self.goal)
			waiting = self.move_base.wait_for_result(rospy.Duration(300))

			self.soundhandle.say("Destination reached", self.voice)
			rospy.sleep(1)
			self.soundhandle.say("Releasing object", self.voice)
			rospy.sleep(1)
			self.soundhandle.say("Exiting the arena", self.voice)
		
			self.goal.target_pose.pose = locations['D']
			self.move_base.send_goal(self.goal)
			waiting = self.move_base.wait_for_result(rospy.Duration(300))

			if waiting == 1:
				self.soundhandle.say("End of routine", self.voice)
				start = 150

	if step == 200:
	    self.soundhandle.say("Goodbye", self.voice)
            rospy.sleep(1)
		## MOVING TO EXIT POINT ##
	    self.goal.target_pose.pose = Pose(Point(-4.2786050667, -3.02841219828, 0.0), Quaternion(0.0, 0.0, -0.5400172439, 0.841653952816))
	    self.move_base.send_goal(self.goal)
	    waiting = self.move_base.wait_for_result(rospy.Duration(300))
		## END ##

    def DigitalInputEventCallback(self, data):
    	global digital_in
	global stop
    	digital_in = data.values
	if digital_in[3] == True:	# emergency button pressed
	    stop = 1
	    rospy.loginfo("Button RED pressed !!!")
	elif digital_in[3] == False:	# emergency button not pressed
	    stop = 0
	    rospy.loginfo("Safe")

    def cleanup(self):
        rospy.loginfo("Shutting down navigation	....")
	self.move_base.cancel_goal()
        self.cmd_vel_pub.publish(Twist())

if __name__=="__main__":
    rospy.init_node('gpsr')
    try:
        B_Function()
        #rospy.spin()
    except:
        pass


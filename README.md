# General Purpose Service Robot (2015)

Intergration code and launch files written in ROS Python for Robocup@Home Hefei, China. 

Full info of the task under 5.1. General Purpose Service Robot:
[Robocup@Home 2015 Rulebook](https://docs.google.com/viewer?a=v&pid=sites&srcid=cm9ib2N1cGF0aG9tZS5vcmd8cm9ib2N1cC1ob21lfGd4OjMyNGMwNTVlZTQ1ZWU2NTc)

